﻿<!DOCTYPE html>
<html lang="en">
<head>
    <head>
		<?php include("head.php");?>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="https://gitlab.com/prof.fabian.lopez/datatablearchivosplanoscrud.git" target="_blank">EDI II</a>
                   
                   
                </div>
            </div>
            <!-- /navbar-inner -->
        </div><br />

            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="content">
                            <?php
								error_reporting(E_ALL);

             					if(isset($_GET['action']) == 'delete'){
									$id_delete = intval($_GET['id']);
/// aqui va la logica de eliminar
// recorre el archivo , guarda en $linea la linea del mismo
// y luego compara lo que se encuentran en la posición 2, que es el DNI con el $_GET que contiene
// el Documento pasado por parametros --> sugerencia se podría pasar encriptado para mejorar
// si no es el que buscamos graba en el archivo nuevo, cuando es igual lo saltea y coloca en true
// la bandera para de borrado para indicar q lo ha encontrado
// cierra los archivos de trabajo y temporal, con unlink borra el anterior y con rename
// renombra con el que trabajamos y lo deja como nuevo archivo de datos real, siempre que pueda realizarlo
// tanto unlink como rename devuelve true si pudo realizar la acción

									$delete = false;
									$myfile = fopen("Padron.csv", "r") or die("No se puede abrir el archivo!");
									$bkfile = fopen("Padron.dat", "w+") or die("No se puede abrir el archivo de trabajo!");
									while(!feof($myfile)) {
										$linea = fgets($myfile);
										$datos=explode("|", $linea);
										
										if (strcmp(trim($datos[2]),trim($_GET['id']))!=0 )
										{
											fputs($bkfile,$linea);
										}
										else{
											$delete = true;
										}
									}
									fclose($myfile);
									fclose($bkfile);
									if (unlink ("Padron.csv")){
										rename ("Padron.dat","Padron.csv");	
									}
									else{
										echo '<div class="alert alert-primary alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error en el borrado, error en proceso de archivo..</div>';		
									}
								
////					
									if($delete){
											echo '<div class="alert alert-primary alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Los datos han sido eliminados correctamente.</div>';
									}else{
											echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar el Votante '.$id_delete.'</div>';
									}
								}					

							?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon-user"></i> DataTable con archivos planos && CRUD </h3> 
						 
                        </div>
						
                        <div class="panel-body">
							<div class="pull-right">
								<a href="registro.php" class="btn btn-sm btn-primary">Nuevo Votante</a>
							</div><br>
							<hr>
                                    <table id="lookup" class="table table-bordered table-hover">  
	                                   <thead bgcolor="#eeeeee" align="center">
                                        <tr>
	                                    <th>Sexo</th>
	                                    <th>Tipo DNI </th>
                                        <th>Documento </th>
                                        <th>Apellido </th>
                                        <th>Nombres </th>
	                                    <th>Domicilio</th>
										<th>Mesa</th>
	                                    <th>Escuela</th>
	                                    <th class="text-center"> Acciones </th> 
	  
                                       </tr>
                                      </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                            
                                </div>
                            </div>
                            
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        
        <!--/.wrapper--><br />
        <div class="footer span-12">
            <div class="container">
              <center> <b class="copyright"><a href="https://gitlab.com/prof.fabian.lopez/datatablearchivosplanoscrud.git"> Demo EDI II</a> &copy; <?php echo date("Y")?> DataTables Bootstrap </b></center>
            </div>
        </div>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
        <script src="datatables/jquery.dataTables.js"></script>
        <script src="datatables/dataTables.bootstrap.js"></script>
        <script>
        $(document).ready(function() {
				var dataTable = $('#lookup').DataTable( {
					
				 "language":	{
					"sProcessing":     "Procesando...",
					"sLengthMenu":     "Mostrar _MENU_ registros",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ningún dato disponible en esta tabla",
					"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "Primero",
						"sLast":     "Último",
						"sNext":     "Siguiente",
						"sPrevious": "Anterior"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				},

					"processing": true,
					"serverSide": true,
					"ajax":{
						url :"ajax-grid-data.php", // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".lookup-error").html("");
							$("#lookup").append('<tbody class="employee-grid-error"><tr><th colspan="3">Sin datos, para ser mostrados en el datatable</th></tr></tbody>');
							$("#lookup_processing").css("display","none");
							
						}
					}
				} );
			} );
        </script>
      
    </body>
